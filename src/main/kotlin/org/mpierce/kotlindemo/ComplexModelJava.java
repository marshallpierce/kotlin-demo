package org.mpierce.kotlindemo;

import java.util.Objects;
import javax.annotation.Nonnull;

/**
 * Implementing equals, hashCode(), and toString() is verbose and error prone in Java.
 *
 * See {@link ComplexModelKotlin} for the Kotlin equivalent.
 */
public class ComplexModelJava {
    private final int count;
    private final String flavor;

    public ComplexModelJava(int count, @Nonnull String flavor) {
        this.count = count;
        this.flavor = flavor;
    }

    public int getCount() {
        return count;
    }

    @Nonnull
    public String getFlavor() {
        return flavor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        ComplexModelJava that = (ComplexModelJava) o;
        return count == that.count &&
                flavor.equals(that.flavor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, flavor);
    }

    @Override
    public String toString() {
        return "ClassWithEqualsEtcJ{" +
                "count=" + count +
                ", flavor='" + flavor + '\'' +
                '}';
    }
}
