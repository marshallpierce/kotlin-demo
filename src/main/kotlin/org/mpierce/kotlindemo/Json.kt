package org.mpierce.kotlindemo

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

/**
 * Integration with Jackson, a common Java JSON library, is easy.
 */
fun main() {
    val mapper = ObjectMapper().apply {
        // In this `apply` block, `this` is the newly constructed ObjectMapper().
        // This allows us to avoid having a local var that ever refers to a partially configured object.
        // Also it makes "fluent-style" methods superfluous -- their syntactic convenience
        // now only requires an `apply` and regular old methods.
        registerModule(KotlinModule())
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
    }

    // typical Jackson usage
    val fruit = mapper.reader()
            .forType(Fruit::class.java)
            // unused property, but no error because of the above config
            .readValue<Fruit>("""{"color":"red", "weight_g":1234, "sku":"83891823"}""")

    // it worked!
    println(fruit)
}

data class Fruit(
        @JsonProperty("color") val color: String,
        @JsonProperty("weight_g") val grams: Int
)
