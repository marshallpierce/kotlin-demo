package org.mpierce.kotlindemo

/**
 * The `data` keyword means "implement equals, hashCode(), and toString(), and also generate a `copy()` method".
 *
 * See [ComplexModelJava] for the Java equivalent
 */
data class ComplexModelKotlin(val count: Int,
                              val flavor: String)

fun main() {
    // you can use named parameters rather than positional if you like
    val thing = ComplexModelKotlin(flavor = "delicious", count = 1234)

    // make a copy with some fields changed
    val thing2 = thing.copy(flavor = "avocado")
    // and another copy with the original flavor
    val thing3 = thing2.copy(flavor = "delicious")

    // toString() for free
    println(thing)
    println(thing2)
    // equals() (and hashCode()) for free
    println(thing == thing3)
}
