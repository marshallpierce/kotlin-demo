package org.mpierce.kotlindemo

/**
 * In kotlin, `val` (with no `private`) creates a public property with a backing field.
 *
 * See [SimpleModelJava] for the Java equivalent.
 */
class SimpleModelKotlin(val name: String)

// top level main function -- not a static method in a class
fun main() {
    val k = SimpleModelKotlin("kotlin")
    val j = SimpleModelJava("java")

    // `if` is an expression
    val str = if (k.name == j.name) {
        "matches"
    } else {
        "doesn't match"
    }

    println(str)
}
