package org.mpierce.kotlindemo

/**
 * See https://kotlinlang.org/docs/reference/null-safety.html
 */
fun main() {
    // nullable type `String?`
    val nullableString: String? = null
    // type is `String`
    val string = "asdf"

    // compiler forces you to null check
    if (nullableString != null) {
        // no null checks needed in here since we've already done it
        // Kotlin calls this a "smart cast"
        if (nullableString.length % 2 == 0) {
            println("Even length")
        } else {
            println("Odd length")
        }
    } else {
        println("Null")
    }

    // Shortcut version of the above logic
    // Uses safe call operator ?. and the elvis operator ?:
    if ((nullableString?.length ?: 0) % 2 == 0) {
        println("Even length")
    } else {
        println("Odd length")
    }

    // or null check with a closure to run if it's non-null
    nullableString?.let { println("nonnull length in let(): ${it.length}") }

    // won't compile unless you add a !! assertion
//    println("length: ${nullableString.length}")

    // warning because it can never be null
    if (string == null) {
        println("Inconceivable")
    }
}
