package org.mpierce.kotlindemo

fun main() {
    val sumOfSquares = (1..10)
            .map { it * it }
            .sum()

    // compiler is not guaranteeing this is exhaustive
    when (sumOfSquares % 2) {
        0 -> println("sum is even")
        1 -> println("sum is odd")
    }

    for (t in listOf("1234", 4567, true)) {
        // `when` used as an expression, so it must handle every case
        val msg = when (t) {
            is String -> "got a string: $t"
            is Int -> "got an int: $t"
            else -> "got something else: $t"
        }

        println(msg)
    }
}
