package org.mpierce.kotlindemo;

/**
 * A private field requires a ctor parameter and usually a getter.
 *
 * See {@link SimpleModelKotlin} for the Kotlin equivalent
 */
public class SimpleModelJava {

    private final String name;

    public SimpleModelJava(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static void main(String[] args) {
        SimpleModelKotlin k = new SimpleModelKotlin("kotlin");
        SimpleModelJava j = new SimpleModelJava("java");

        if (k.getName().equals(j.getName())) {
            System.out.println("matches");
        } else {
            System.out.println("doesn't match");
        }
    }
}
