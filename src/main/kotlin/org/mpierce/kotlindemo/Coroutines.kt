package org.mpierce.kotlindemo

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.time.Duration
import java.time.Instant
import java.util.concurrent.Executors
import java.util.concurrent.SynchronousQueue

/**
 * This implements the same workload with both threads and coroutines.
 *
 * There are two queues: input and output. The threads (or coroutines) pull one item from the input queue and use it to create an object that is placed into the output queue.
 *
 * On a 32gb RAM macOS system with default JDK 11 settings, 5k threads works (in about 2.7s) but 10k does not (crashes with OOME).
 */
fun main() {
    val num = 500

    runThreads(num)
    runThreads(num * 10)

    runBlocking {
        runCoroutines(num)
        runCoroutines(num * 10)
        runCoroutines(num * 100)
        runCoroutines(num * 1_000)
        runCoroutines(num * 10_000)
    }
}

private fun runThreads(num: Int) {
    val input = SynchronousQueue<Int>()
    val output = SynchronousQueue<String>()

    val beforeLaunch = Instant.now()

    val threads = (1..num)
            .map { n ->
                val t = Thread {
                    val i = input.take()
                    output.put("Thread $n got $i")
                }
                t.start()
                t
            }

    val beforeInput = Instant.now()

    repeat(num) {
        input.put(it)
    }

    repeat(num) {
        output.take()
    }

    val afterOutput = Instant.now()

    for (t in threads) {
        t.join()
    }

    println("$num threads: ${Duration.between(beforeLaunch, beforeInput)} startup, ${Duration.between(beforeInput,
            afterOutput)} input -> output, ${Duration.between(afterOutput, Instant.now())} shutdown")
}

private suspend fun runCoroutines(num: Int) {
    val input = Channel<Int>()
    val output = Channel<String>()

    val beforeLaunch = Instant.now()

    // Put the jobs on another thread to be more like the Java example.
    // They could run on the same thread though
    val dispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher()
    val jobs = (1..num)
            .map { n ->
                GlobalScope.launch(dispatcher) {
                    val i = input.receive()
                    output.send("Coroutine $n got $i")
                }
            }

    val beforeInput = Instant.now()

    repeat(num) {
        input.send(it)
    }

    repeat(num) {
        output.receive()
    }

    val afterOutput = Instant.now()

    for (j in jobs) {
        j.join()
    }

    dispatcher.close()

    println("$num coroutines: ${Duration.between(beforeLaunch, beforeInput)} startup, ${Duration.between(beforeInput,
            afterOutput)} input -> output, ${Duration.between(afterOutput, Instant.now())} shutdown")
}
