package org.mpierce.kotlindemo

sealed class ExternalCredentials(val internalId: Int)

// multiple top level classes are OK in Kotlin, unlike in Java where it breaks incremental compilation
class AwsCredentials(internalId: Int, val accessKeyId: String) : ExternalCredentials(internalId)

class GcpCredentials(internalId: Int, val privateKeyJson: String) : ExternalCredentials(internalId)

fun loadCredentials(): ExternalCredentials {
    return GcpCredentials(1, """{"secret":"json"}""")
}

fun main() {
    val creds = loadCredentials()

    // can always access a field in the parent class
    println("id ${creds.internalId}")

    // not required to be exhaustive here because the `when` expression evaluation isn't assigned or used
    when (creds) {
        // can access per-subclass fields inside each `is` clause
        is AwsCredentials -> println("AWS access key id: ${creds.accessKeyId}")
        is GcpCredentials -> println("GCP json: ${creds.privateKeyJson}")
    }

    // has to be exhaustive here
    val hasRegionInIowa = when (creds) {
        is AwsCredentials -> false
        is GcpCredentials -> true
    }

    println("Has region in Iowa: $hasRegionInIowa")
}
