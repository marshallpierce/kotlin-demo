Just a simple demo of some basic Kotlin features.

You can run `./gradlew build` to compile everything, but you probably want to open the project in IntelliJ to play with the examples.

- Simple classes with fields (Java vs Kotlin)
- Classes with `equals()`, `hashCode()`, `toString()` (Java vs Kotlin)
- Nullability as part of the type system
- `when` expressions
- Sealed classes
- Handling JSON
- Coroutines vs threads
